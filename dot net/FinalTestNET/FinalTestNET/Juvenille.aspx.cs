﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalTestNET
{
    public partial class Juvenille : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DatabaseConnection connect = new DatabaseConnection();
            connect.JuvenileExecuteInsert(
                                        tbLastName.Text,
                                        tbFirstName.Text,
                                        tbMiddeName.Text,
                                        Convert.ToInt32(tbAdult.Text),
                                        Convert.ToDateTime(tbDOB.Text)
                                        );
            Response.Write("Record was successfully added!");
            ClearControls(Page);
        }
        public static void ClearControls(Control Parent)
        {

            if (Parent is TextBox)
            { (Parent as TextBox).Text = string.Empty; }
            else
            {
                foreach (Control c in Parent.Controls)
                    ClearControls(c);
            }

        }
    }
}