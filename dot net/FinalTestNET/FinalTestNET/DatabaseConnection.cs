﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FinalTestNET
{
    public class DatabaseConnection
    {
        public string GetConnectionString()
        {
            //sets the connection string from your web config file "ConnString" is the name of your Connection String
            return System.Configuration.ConfigurationManager.ConnectionStrings["MyConsString"].ConnectionString;
        }

        public void AdultExecuteInsert(string lastname, string firstname, string middleinitial, string street, string city, string state, string zip, string phone, DateTime expiry)
        {
            SqlConnection conn = new SqlConnection(GetConnectionString());
            string sql = "[dbo].[InsertData]";
            int age = 1;

            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlParameter[] param = new SqlParameter[10];
                //param[0] = new SqlParameter("@id", SqlDbType.Int, 20);
                param[0] = new SqlParameter("@lastname", SqlDbType.VarChar, 40);
                param[1] = new SqlParameter("@firstname", SqlDbType.VarChar, 40);
                param[2] = new SqlParameter("@middleinitial", SqlDbType.VarChar, 40);
                param[3] = new SqlParameter("@street", SqlDbType.VarChar, 50);
                param[4] = new SqlParameter("@city", SqlDbType.VarChar, 50);
                param[5] = new SqlParameter("@state", SqlDbType.VarChar, 2);
                param[6] = new SqlParameter("@zip", SqlDbType.VarChar, 10);
                param[7] = new SqlParameter("@phone", SqlDbType.VarChar, 10);
                param[8] = new SqlParameter("@age", SqlDbType.Int);
                param[9] = new SqlParameter("@expr_date", SqlDbType.DateTime);


                param[0].Value = lastname;
                param[1].Value = firstname;
                param[2].Value = middleinitial;
                param[3].Value = street;
                param[4].Value = city;
                param[5].Value = state;
                param[6].Value = zip;
                param[7].Value = phone;
                param[8].Value = age;
                param[9].Value = expiry;

                for (int i = 0; i < param.Length; i++)
                {
                    cmd.Parameters.Add(param[i]);
                }

                cmd.CommandType = CommandType.StoredProcedure;
                //int result = Convert.ToInt32(cmd.ExecuteScalar);
                //int result = (int)cmd.ExecuteScalar();
                cmd.ExecuteScalar();
                //return result;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "Insert Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                conn.Close();
            }
        }
        public void JuvenileExecuteInsert(string lastname, string firstname, string middleinitial, int adult_member_id, DateTime birth_date)
        {
            SqlConnection conn = new SqlConnection(GetConnectionString());
            string sql = "[dbo].[InsertData]";
            int age = 2;

            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlParameter[] param = new SqlParameter[6];
                //param[0] = new SqlParameter("@id", SqlDbType.Int, 20);
                param[0] = new SqlParameter("@lastname", SqlDbType.VarChar, 40);
                param[1] = new SqlParameter("@firstname", SqlDbType.VarChar, 40);
                param[2] = new SqlParameter("@middleinitial", SqlDbType.VarChar, 40);
                param[3] = new SqlParameter("@adult_member_id", SqlDbType.Int);
                param[4] = new SqlParameter("@birth_date", SqlDbType.DateTime);
                param[5] = new SqlParameter("@age", SqlDbType.Int);


                param[0].Value = lastname;
                param[1].Value = firstname;
                param[2].Value = middleinitial;
                param[3].Value = adult_member_id;
                param[4].Value = birth_date;
                param[5].Value = age;

                for (int i = 0; i < param.Length; i++)
                {
                    cmd.Parameters.Add(param[i]);
                }

                cmd.CommandType = CommandType.StoredProcedure;
                //int result = Convert.ToInt32(cmd.ExecuteScalar);
                //int result = (int)cmd.ExecuteScalar();
                cmd.ExecuteScalar();
                //return result;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "Insert Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}