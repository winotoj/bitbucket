﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalTestNET
{
    public partial class AdultRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DatabaseConnection connect = new DatabaseConnection();
            connect.AdultExecuteInsert(tbLastName.Text,
                                        tbFirstName.Text,
                                        tbMiddleName.Text,
                                        tbStreet.Text,
                                        tbCity.Text,
                                        tbState.Text,
                                        tbZip.Text,
                                        tbPhone.Text,
                                        Convert.ToDateTime(tbExpiry.Text));
            Response.Write("Record was successfully added!");
            ClearControls(Page);


        }
        public static void ClearControls(Control Parent)
        {

            if (Parent is TextBox)
            { (Parent as TextBox).Text = string.Empty; }
            else
            {
                foreach (Control c in Parent.Controls)
                    ClearControls(c);
            }

        }
    }
}