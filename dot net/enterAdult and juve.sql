use Library;
go

drop procedure insertdata
;
go

create Procedure InsertData
@lastname varchar(40),
@firstname varchar(40),
@middleinitial varchar (40),
@street varchar(50) = null,
@city varchar(50) = null,
@state varchar(2) = null,
@zip varchar(10) = null,
@phone varchar(10) = null,
@member_id int = null,
@adult_member_id int = null,
@birth_date datetime = null,
@expr_date datetime = null,	
@age int

as


begin
if (@age = 1)
	begin
		insert into person.Member (lastname, firstname, middleinitial)
		values (@lastname, @firstname, @middleinitial)

		insert into person.Adult (member_id, street, city, state, zip, phone, expr_date)
		values (scope_identity(), @street, @city, @state, @zip, @phone, @expr_date)
	end
else if (@age = 2)
	begin
		insert into person.member (lastname, firstname, middleinitial)
		values (@lastname, @firstname, @middleinitial)
		insert into person.Juvenile (member_id, adult_member_id, birth_date)
		values (SCOPE_IDENTITY(), @adult_member_id, @birth_date)
	end

end
;
go
