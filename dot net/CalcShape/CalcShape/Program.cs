﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcShape
{

    class Shape
    {
        protected double width;
        protected double heigth;
        protected double radius;
        public void setWidth(double w)
        {
            width = w;
        }
        public void setHeigth(double h)
        {
            heigth = h;
        }
        public void setRadius(double r)
        {
            radius = r;
        }
    }
    class Rectangle: Shape
    {
        public double GetArea()
        {
            return width * heigth;
        }
    }
    class Triangle: Shape
    {
        public double GetArea()
        {
            return 0.5 * width * heigth;
        }
    }
    class Circle: Shape
    {
        public double GetArea()
        {
            return (22 / 7)*(radius * radius);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("format: r|t|c (rectangle, triangle, circle) width and height, or radius for circle");
                Console.ReadKey();
            }
            if (args.Length == 2)
            {
                double radius;
                Circle circle = new Circle();
                double.TryParse(args[1], out radius);
                circle.setRadius(radius);
                circle.GetArea();
                Console.WriteLine("The area is: {0}", circle.GetArea());
                Console.ReadKey();
            }
            else if (args.Length == 3)
            {
                if (args[0] == "t")
                {
                    Triangle triangle = new Triangle();
                    double width, height;
                    double.TryParse(args[1], out width);
                    double.TryParse(args[2], out height);
                    triangle.setWidth(width);
                    triangle.setHeigth(height);
                    Console.WriteLine("The area is: {0}", triangle.GetArea());
                    Console.ReadKey();
                }
                else if (args[0] == "r")
                {
                    Rectangle rectangle = new Rectangle();
                    double width, heigth;
                    double.TryParse(args[1], out width);
                    double.TryParse(args[2], out heigth);
                    rectangle.setWidth(width);
                    rectangle.setHeigth(heigth);
                    Console.WriteLine("The area is: {0}", rectangle.GetArea());
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("Wrong shape");
                    Console.ReadKey();
                }
            }
            else
            {
                Console.WriteLine("You need to pass 2 or 3 args");
                Console.ReadKey();
            }
        }
    }
}
