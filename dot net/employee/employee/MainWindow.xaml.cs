﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace employee
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void DisplayPic(string fName)
        {
            String stringPath = fName;
            Uri imageUri = new Uri(stringPath, UriKind.Relative);
            BitmapImage imageBitmap = new BitmapImage(imageUri);
            img.Source = imageBitmap;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (lb1.IsSelected)
            {
                lblName.Content = lb1.Content;
                DisplayPic("img1.jpg");           
            }
            else if (lb2.IsSelected)
            {
                lblName.Content = lb2.Content;
                DisplayPic("img2.jpg");
            }
            else if (lb3.IsSelected)
            {
                lblName.Content = lb3.Content;
                DisplayPic("img3.jpg");
            }
            else if (lb4.IsSelected)
            {
                lblName.Content = lb4.Content;
                DisplayPic("img4.jpg");
            }
            else if (lb5.IsSelected)
            {
                lblName.Content = lb4.Content;
                DisplayPic("img5.jpg");
            }
        }
    }
}
