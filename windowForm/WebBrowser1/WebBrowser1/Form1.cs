﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebBrowser1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            wbBrowser.Navigate(new Uri(cbWeb.SelectedItem.ToString()));
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            wbBrowser.GoHome();
        }

        private void goBackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            wbBrowser.GoBack();
        }

        private void goForwardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            wbBrowser.GoForward();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbWeb.SelectedIndex = 0;
            wbBrowser.GoHome();
        }
    }
}
